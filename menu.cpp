#include <string.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>

#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>

#include "menu.h"


Menu::Menu(int argc, char* argv[]) {
	this->argc = argc;
	this->argv = argv;

	this->daemon = true;
	this->help = false;
	
}

void Menu::checkArguments() {
	for(int i=1; i < this->argc; i++) {
		if(strcmp(argv[i], "-D") == 0) {
			this->setDaemon( false);
		}

		// help flag setzen wenn parameter uebergeben wurde
		if(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
			this->help = true;
			break;
		}
	
		// nick setzen, wenn parameter uebergeben wurde
		if(strcmp(argv[i], "-n") == 0) {
			this->nick = argv[i+1];
		}

		// server übernehmen, wenn paramter übergeben wurde
		if(strcmp(argv[i], "-s") == 0) {
			this->server = argv[i+1];
		}

		// passwort flag setzen, wenn parameter übergeben wurde
		if(strcmp(argv[i], "-p") == 0) {
			this->port = (int) argv[i+1];
		}

		if(strcmp(argv[i], "-c") == 0) {
			this->channel = argv[i+1];
		}
	}
}

bool Menu::getHelp() {
	if(this->help) {
		std::ifstream f;
		std::string s;
		f.open("/usr/local/ircbot/help.txt", std::ios::in);
		while(!f.eof()) {
			getline(f, s);
			std::cout << s << std::endl;
		}
		f.close();
		exit(0);
	}
}

// setter & getter functions
bool Menu::getDaemon() {
	return this->daemon;
}

void Menu::setDaemon(bool d) {
	this->daemon = d;
}

char* Menu::getNick() {
	return this->nick;
}

void Menu::setNick(char* nick) {
	this->nick = nick;
}

char* Menu::getChannel() {
	return this->channel;
}

void Menu::setChannel(char* chan) {
	this->channel = chan;
}

char* Menu::getServer() {
	return this->server;
}

void Menu::setServer(char* server) {
	this->server = server;
}

int Menu::getPort() {
	return this->port;
}

void Menu::setPort(int p) {
	this->port = p;
}
