#ifndef __IRCBOT_H__
#define __IRCBOT_H__

#include <string>

#include "include/libircclient.h"

using namespace std;

class Ircbot {
public:
	Ircbot();
	int checkMessage(irc_session_t* session, const char** params);
	int lastSeen(string nickname);
	int sendMail();

	char* mail;
	char* cmd;
	char* nick;

private:
	string result;
	
};

#endif
