#ifndef __IRCBOT_H_
#define __IRCBOT_H_

#include "include/libircclient.h"

class Menu {
public:
	Menu(int argc, char* argv[]);
	void checkArguments();

	bool getHelp();
	void setHelp(bool b);
	bool getDaemon();
	void setDaemon(bool b);


	char* getNick();
	void setNick(char* nick);
	char* getChannel();
	void setChannel(char* chan);
	char* getServer();
	void setServer(char* server);
	int getPort();
	void setPort(int port);

private:
	char** argv;
	int argc;
	bool daemon;
	bool help;
	char* server;
	int port;
	char* nick;
	char* channel;

	
};

#endif
