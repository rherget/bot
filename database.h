#ifndef __DBSQLITE__
#define __DBSQLITE__

#include <sqlite3.h>
#include <string>

class Sqlitedb {

public:
	Sqlitedb();
	~Sqlitedb();
	bool connectToDB();
	bool insert(char* query);
	bool addJoinEvent(const char* event, const char* origin, const char** params);
	bool logJoin(const char* event, const char* origin, const char** params);
	bool updateJoinEvent(const char* event, const char* origin, const char** params);
	void logEvent(const char* event, const char* origin, const char** params);

	const char* lastSeen(std::string username);
	bool createLogFile();

	char* showJoinEvent();
	bool isExists(const char* origin);
	
	std::string query;
	char** result;
	int rc;
	int row, col;
	char* db_err;
private:
	sqlite3 *db;

};


#endif
