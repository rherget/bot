create table if not exists user(
id integer primary key autoincrement,
username varchar(100),
active int(1),
logon text,
logoff text
);

create table if not exists log(
id integer primary key autoincrement,
timestamp text,
log text
);
