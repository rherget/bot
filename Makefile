PROGPATH = /usr/local/ircbot
LIBS = src/libircclient.a -lpthread -lnsl
INCLUDES=-I./include
LDFLAGS += -libircclient -lsqlite

SL3FLAGS = -W -s -pipe -Wl,-R/usr/local/lib -lsqlite3
MANPATH = /usr/share/man/man1
PROGNAME = ircbot

all: main

main: main.o menu.o database.o bot.o
	g++ -o $(PROGNAME) main.o menu.o database.o  bot.o $(SL3FLAGS) $(LIBS)
	make clean

main.o: main.cpp menu.h bot.h

menu.o: menu.cpp menu.h

database.o: database.cpp database.h

bot.o: bot.cpp bot.h

install:
	apt-get install sqlite3
	apt-get install libsqlite3-dev
	apt-get install sendemail		
	mkdir -p $(PROGPATH)
	sqlite3 ircbotdb < config/createdb.sql
	cp help.txt $(PROGPATH)/help.txt
	mv ircbotdb $(PROGPATH)/ircbotdb
	doxygen
	mv $(PROGNAME) $(PROGPATH)maker
	make manpage

manpage:
	gzip -c config/ircbot.1 > config/ircbot.1.gz
	mv config/ircbot.1.gz $(MANPATH)/$(PROGNAME).1.gz

uninstall:
	rm -rf $(PROGPATH)
	rm $(MANPATH)/$(PROGNAME).1.gz

clean:
	rm *.o
