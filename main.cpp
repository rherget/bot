#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>

#include <sqlite3.h>

#include "database.h"
#include "bot.h"
#include "menu.h"
#include "include/libircclient.h"

using namespace std;

typedef struct
{
	char* channel;
	char* nick;
} irc_ctx_t;

Sqlitedb* db = new Sqlitedb();

void eventConnect (irc_session_t* session, const char* event, const char* origin, const char** params, unsigned int count)
{
	irc_ctx_t* ctx = (irc_ctx_t*) irc_get_ctx(session);
	irc_cmd_join (session, ctx->channel, 0); 
	db->logEvent(event, origin, params);
}

void eventJoin (irc_session_t* session, const char* event, const char* origin, const char** params, unsigned int count)
{
	db->logJoin(event, origin, params);
}

void eventChannel (irc_session_t* session, const char* event, const char* origin, const char** params, unsigned int count)
{
	db->logEvent(event, origin, params);

	Ircbot* bot = new Ircbot();
	int action = bot->checkMessage(session, params);

	string log;
	int i;
	switch(action) 
	{
		case 1:
			irc_cmd_me(session, params[0], db->lastSeen(bot->mail));
			break;
		case 2:
			db->createLogFile();
			i = bot->sendMail();
			if(i == 0)
			{
				log = " Logfile wurde an folgende Person gesendet ";
			}
			else
			{
				log = " Logfile konnte an folgende Person nicht gesendet werden ";
			}
			log += bot->mail;
			irc_cmd_me(session, params[0], log.c_str());
			break;
		case 3:
			irc_cmd_me(session, params[0], "hilfe:");
			irc_cmd_me(session,params[0], "folgende Befehle sind möglich");
			irc_cmd_me(session, params[0], " ");
			
			log = bot->nick;
			log += " Wo ist <nick>  -  <nick> wurde zuletzt gesehen";
			irc_cmd_me(session, params[0], log.c_str());

			log = bot->nick;
			log += " sende <mailadress>  -  sende logfile per Mail";
			irc_cmd_me(session, params[0], log.c_str());
			
			log = bot->nick;
			log += " about  -  Informationen über mich";
			irc_cmd_me(session, params[0], log.c_str());
			break;
		default:
			break;
	}
}

// Daemone
bool daemonize() 
{
	int i, lfp;
	char str[10];
	
	if(getppid() == 1) return false;
	i = fork();
	if(i < 0) exit(1);
	if(i > 0) exit(0);
	
	setsid();
	for(i-getdtablesize(); i>=0; --i) close(i);
	i=open("/dev/null", O_RDWR); dup(i); dup(i);

	sprintf(str, "%d\n", getpid());
	write(lfp, str, strlen(str));

	signal (SIGCHLD, SIG_IGN);
	signal (SIGTSTP, SIG_IGN);
	signal (SIGTTOU, SIG_IGN);
	signal (SIGTTIN, SIG_IGN);
	signal (SIGHUP, SIG_IGN);
	signal (SIGTERM, SIG_DFL);
}

// SQL Datenbank callback
static int callback(void *NotUsed, int argc, char **argv, char **azColName) 
{
	int i;
	for(i=0; i<argc; i++)	
	{
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

int main(int argc, char** argv)
{
	if((chdir("/usr/local/ircbot") < 0)) exit(1);
	Sqlitedb* db;
	irc_callbacks_t callbacks;
	irc_session_t *s;
	irc_ctx_t ctx;
	db = new Sqlitedb();
	int rc;

	// Optionen abfragen (Parameter)
	Menu *options = new Menu(argc, argv);

	// Standard IRC
	options->setServer((char*) "irc.quakenet.org");
	options->setPort(6667);
	options->setChannel((char*) "#linuxprog");
	options->setNick((char*) "IRCBot");

	// Argumente uebernehmen
	options->checkArguments();
	options->getHelp();


	if(options->getDaemon()) 
	{
		cout << "Wird als Daemon gestartet..." << endl;
		daemonize();
	}
	
	// Erstellung des Callbacks
	memset (&callbacks, 0, sizeof(callbacks));


	callbacks.event_connect = eventConnect;
	callbacks.event_join    = eventJoin;
	callbacks.event_part	= eventJoin;
	callbacks.event_kick	= eventJoin;
	callbacks.event_channel = eventChannel;

  
	ctx.channel = options->getChannel();
	ctx.nick    = options->getNick();

	s = irc_create_session(&callbacks);
	if (!s)
	{
		printf("Couldn't set up da session...\n");
		return 1;
	}


	irc_set_ctx(s, &ctx);
	irc_option_set(s, LIBIRC_OPTION_STRIPNICKS);

	// Verbindung herstellen
	cout << "Verbinden zu " << options->getServer() << "..." << endl;
	if ( irc_connect(s, options->getServer(), options->getPort(), 0, options->getNick(), 0, 0))
	{
		cout<< "Konnte keine Verbindung herstellen: " << options->getServer() << endl;
		return 1;
	}
	else
	{
		cout << "Verbunden zu Server " << options->getServer() << endl;
	}

  	if( irc_run(s))
	{
		cout << "Error: " << irc_strerror(irc_errno(s)) << endl;
		return 1;
  	}
  
	int chan = irc_cmd_channel_mode(s, options->getChannel(), NULL);
	if(chan == 0)
	{
		cout << "Channel wurde nicht gewechselt" << endl;
		return 1;
	}
	else
	{
		cout << "Verbunden zu Channel " << options->getChannel() << endl;
	}

	while(true)
	{
		sleep(10);
	}
	return 0;
}
