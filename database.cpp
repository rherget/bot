#include <stdio.h>
#include "database.h"
#include <sqlite3.h>
#include <string.h>
#include <fstream>
#include <stdlib.h>
#include <iostream>

#include "include/libircclient.h"

using namespace std;

Sqlitedb::Sqlitedb () 
{
	sqlite3_open("/usr/local/ircbot/ircbot", &this->db);
}

// Datenbankconnection aufbauen
bool Sqlitedb::connectToDB()
{
	sqlite3_open("ircbot", &this->db);
	return true;
}

// Neues Event erstellen
bool Sqlitedb::addJoinEvent(const char* event, const char* origin, const char** params)
{
	string insert, insertend;
	if(strcmp(event, "JOIN") == 0) 
	{
		insert = "insert into user(username, active, logon, logoff) values('";
		insertend = "',1,datetime('now','LOCALTIME'),datetime('now','LOCALTIME'));";
	} 
	else 
	{
		insert = "insert into user(username, active, logon, logoff) values('";
		insertend = "',0,datetime('now','LOCALTIME'),datetime('now','LOCALTIME'));";
	}

	insert += origin;
	insert += insertend;
	sqlite3_exec(this->db, insert.c_str(), NULL, 0, &this->db_err);
}

// Log JoinEvent
bool Sqlitedb::logJoin(const char* event, const char* origin, const char**params) 
{
	if(this->isExists(origin)) 
	{
		this->updateJoinEvent(event,origin, params);
		return true;
	}
	this->addJoinEvent(event, origin, params);
	return true;
}

// Update eines Events
bool Sqlitedb::updateJoinEvent(const char* event, const char* origin, const char** params) 
{
	string update;
	if(strcmp(event, "JOIN") == 0) 
	{
		update = "update user set active=1, logon=datetime('now','LOCALTIME') where username='";
	} 
	else
	{
		update = "update user set active=0, logoff=datetime('now','LOCALTIME') where username='";
	}
	update += origin;
	update += "';";
	sqlite3_exec(this->db, update.c_str(), NULL, 0, &this->db_err);
}

char* Sqlitedb::showJoinEvent() 
{
	this->query = "select username, logon from user;";
	sqlite3_get_table(this->db, this->query.c_str(), &this->result, &this->row, &this->col, &this->db_err);
}

// Ist Name bereits vorhanden?
bool Sqlitedb::isExists(const char* origin)
{
	this->query = "select * from user where username ='";
	this->query += origin;
	this->query += "';";
	sqlite3_get_table(this->db, this->query.c_str(), &this->result, &this->row, &this->col, &this->db_err);
	if(this->row == 0) return false;
	return true;
}

// EventLogger
void Sqlitedb::logEvent(const char* event, const char* origin, const char** params)
{
	this->query = "insert into log(log, timestamp) values ('";
	this->query += event;
	this->query += ", ";
	this->query += origin;
	this->query += " (";
	this->query += params[0];
	this->query += ")";
	if(params[1])
	{
		this->query += ": ";
		this->query += params[1];
	}
	this->query += "', datetime('now','LOCALTIME'));";
	
	int i = sqlite3_exec(this->db, this->query.c_str(), NULL, 0, &this->db_err);
}

// Gebe letztes Datum vom gesehenen User
const char* Sqlitedb::lastSeen(string username) 
{
	this->query = "select active, logoff from user where username='";
	this->query += username;
	this->query += "';";
	string msg = username;

	sqlite3_get_table(this->db, this->query.c_str(), &this->result, &this->row, &this->col, &this->db_err);
	
	if(this->row == 0) 
	{
		msg += " ist nicht hinterlegt.";
		return msg.c_str();
	}
	if(strcmp(this->result[2], "1") == 0) 
	{
		msg += " ist online :-)";
		return msg.c_str();
	}
	msg += " ist offline gegangen.";
	msg += this->result[3];
	return msg.c_str();
}

// Neues Logfile erstellen
bool Sqlitedb::createLogFile() 
{
	ofstream File("log.txt");
	
	this->query = "select timestamp, log from log;";
	sqlite3_get_table(this->db, this->query.c_str(), &this->result, &this->row, &this->col, &this->db_err);
	
	for(int i = this->col; i < (this->row + 1) * this->col; i = i + this->col)
	{
		File << result[i] << " " << result[i+1] << endl;
	}
	File.close();
}

Sqlitedb::~Sqlitedb() 
{
	sqlite3_close(this->db);
}
